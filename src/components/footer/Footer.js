import FooterLogo from '../../static/images/footer/footer_logo.png'
import {Navbar, Nav, Container, Row, Col} from 'react-bootstrap'
import {SocialIcon} from 'react-social-icons'

import '../../fonts/Gilroy-Regular.ttf'

import './Footer.css'

function Footer() {
    return (
        <Container className="mt-3 ml-1 mr-1">
            <Row>
                <Col sm={3}>
                    <img
                        alt="logo"
                        src={FooterLogo}
                        width={1588 / 10}
                        height={638 / 10}
                        className="d-inline-block align-top"
                    />
                </Col>
                <Col sm={2}></Col>
                <Col sm={2}>
                    <p className="footer-text ">Privacy Policy</p>
                </Col>
                <Col sm={3}>
                    <p className="footer-text">Terms & Conditions</p> 
                    <p className="footer-text">Cookies</p>
                </Col>
                <Col sm={2}>
                    <p className="footer-text">Contact Us</p>
                </Col>
            </Row>
            <Row>
                <Col sm={5}>
                    <p>2020 1buzz copyrights reserved</p>
                </Col>
                {/* <Col sm={2}></Col> */}
                <Col sm={7}>
                    <Row>
                        <Col xs={3}></Col>
                        <Col xs={7}>
                            <Row>
                                <Col xs={4} style={{ "padding-left": 10, "padding-right": 10, "padding-top": 10, "padding-bottom": 10}}>
                                    <SocialIcon url="https://www.facebook.com/1buzzfest/" style={{ height: 35, width: 35 }}/>
                                </Col>
                                <Col xs={4} style={{ "padding-left": 10, "padding-right": 10, "padding-top": 10, "padding-bottom": 10 }}>
                                    <SocialIcon url="https://www.instagram.com/1buzzfest/?igshid=hucqlzri0xz0" style={{ height: 35, width: 35 }}/>
                                </Col>
                                <Col xs={4} style={{ "padding-left": 10, "padding-right": 10, "padding-top": 10, "padding-bottom": 10 }}>
                                    <SocialIcon url="mailto:hello@1buzzfest.com" style={{ height: 35, width: 35 }}/>
                                </Col>
                            </Row>
                        </Col>
                        <Col xs={2}/>
                    </Row>
                </Col>
            </Row>
        </Container>
    )
}

export default Footer