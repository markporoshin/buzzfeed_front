import HeaderLogo from '../../static/images/header/header_logo_gif.gif'
import {Navbar, Nav, Image} from 'react-bootstrap'
import {SocialIcon} from 'react-social-icons'
import {Container, Row, Col} from 'react-bootstrap'

import './Header.css'

import '../../fonts/DoctorGlitch.ttf'

function Header({menuCallback}) {

    const handleMenuClick = (pageName)=> {
        console.log("нажата кнопка: " + pageName)
        menuCallback(pageName)
    }

    return (
        <div>
            <Navbar fixed="top" bg="white">
                <Navbar.Brand href="#home">
                <img
                    alt="logo"
                    src={HeaderLogo}
                    width={100}
                    height={100}
                    className="d-inline-block align-top"
                />
                </Navbar.Brand>
                <Nav className="ml-auto">
                    <Nav.Link ><div className="menu-text link" onClick={()=>{handleMenuClick("Our Story")}}>Our Story</div></Nav.Link>
                    <Nav.Link ><div className="menu-text link" onClick={()=>{handleMenuClick("Fests")}}>Fests</div></Nav.Link>
                    <Nav.Link ><div className="menu-text link" onClick={()=>{handleMenuClick("Buzz Us")}}>Buzz Us</div></Nav.Link>
                </Nav>
                
                <Nav className="ml-auto">
                
                    <Container>
                        <Row>
                            <Col xs={3} style={{ "padding-left": 10, "padding-right": 10, "padding-top": 10, "padding-bottom": 10}}>
                                <SocialIcon url="https://www.facebook.com/1buzzfest/" style={{ height: 35, width: 35 }}/>
                            </Col>
                            <Col xs={3} style={{ "padding-left": 10, "padding-right": 10, "padding-top": 10, "padding-bottom": 10 }}>
                                <SocialIcon url="https://www.instagram.com/1buzzfest/?igshid=hucqlzri0xz0" style={{ height: 35, width: 35 }}/>
                            </Col>
                            <Col xs={3} style={{ "padding-left": 10, "padding-right": 10, "padding-top": 10, "padding-bottom": 10 }}>
                                <SocialIcon url="mailto:hello@1buzzfest.com" style={{ height: 35, width: 35 }}/>
                            </Col>
                        </Row>
                    </Container>

                </Nav>
            </Navbar>
            <div style={{height: 120}}/>
        </div>
            
        
    )
}

export default Header