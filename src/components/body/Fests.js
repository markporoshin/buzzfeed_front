import Background from '../../static/images/backgrounds/green.jpg'
import {Card, Container, Col, Row} from 'react-bootstrap'

import './Fest.css'

function Fest() {

    return (
        <div id="maaaark" width="100%" style={{height: '100%'}}>
            <Card className="bg-dark text-white text-center" width="100%" style={{width:"100%"}}>
            <Card.Img src={Background} alt="Card image" width="10" style={{width:"100%"}}/>
            <Card.ImgOverlay style={{width:"100%"}}>
                <Card.Title><div className="title-text">Fest</div></Card.Title>
                <Card.Text>
                <Container>
                    <Row>
                        <Col xs={1} md={2}/>
                        <Col xs={10} md={8}>
                            <div className="text">
                                Our fest will pump up your mood with a sober morning party which
                                includes a variety of fun activities and integrated experiences,

                                We open our doors to support emerging artists and talents 
                                as well as providing a platform for local businesses to be seen and heard
                            </div>
                        </Col>
                        <Col xs={1} md={2}/>
                    </Row>
                </Container>

                <p><button className="orange-button">Learn More</button></p>
                </Card.Text>
            </Card.ImgOverlay>
            </Card>
        </div>
    )
}


export default Fest;