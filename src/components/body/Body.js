import {Container, Row, Col} from 'react-bootstrap';
import React, {useRef, useState, useEffect} from 'react'

import BuzzUs from './BuzzUs'
import Fests from './Fests'
import OurStory from './OurStory'
import ReactPlayer from "react-player";

import './body.css'


function timeout(delay) {
    return new Promise( res => setTimeout(res, delay) );}

const scrollToRef = (ref) => window.scrollTo(0, ref.current.offsetTop)  

function Body({page, scroll}) {
    console.log("scroll: " + scroll)
    const ourstory = useRef(null)
    const fests = useRef(null)
    const buzzus = useRef(null)
    const video = useRef(null)

    const [width, setWidth] = useState(0)
    const [height, setHeight] = useState(0)
  
    useEffect(() => {
      setWidth(video.current.offsetWidth)
      setHeight(video.current.clientHeight)
    })
    const startScrollingToRef = (ref) => {
        window.scrollTo({top: ref.current.offsetTop, left: 0, behavior: 'smooth' })
    }

    switch(page) {
        case "Our Story":
            startScrollingToRef(ourstory)
            break
        case "Fests":
            startScrollingToRef(fests)
            break
        case "Buzz Us":
            startScrollingToRef(buzzus)
            break
    }
    

    

    console.log("width: " + width)
    console.log("height: " + height)
    return (
        <Container fluid width="100%" style={{ "padding-left": 0, "padding-right": 0, "margin-left": "0px", "margin-right": "0px", width:"100%"}}>
            <Row width="100%" ref={video}>
                <div id="parent">
                    <iframe src='https://www.youtube.com/embed/hEnr6Ewpu_U?autoplay=1&mute=1'
                            frameBorder='0'
                            allow='autoplay; encrypted-media'
                            title='video'
                            width={width}
                            height={width * 2 / 3}
                            id="box"
                    />  
                    <button className="overvideobuttom orange-button" 
                        style={{
                            "left": width / 2 - 50 + "px", 
                            "top": width * 2 / 5 + "px",
                            "height": width / 20,
                            "wight": width / 3
                        }}>Subscribe
                        </button>
                </div>
                {/* left: 250px;
    top: 50px; */}
            </Row>
            <Row ref={ourstory}>
                <OurStory  width="100%"/>
            </Row>
            <Row ref={fests}>
                <Fests />
            </Row>
            <Row ref={buzzus}>
                <BuzzUs/>
            </Row>
        </Container>
    )
}


export default Body;