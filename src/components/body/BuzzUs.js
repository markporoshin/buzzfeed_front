import Background from '../../static/images/backgrounds/orange.jpg'
import {Card, Container, Col, Row, Form} from 'react-bootstrap'

import './BuzzUs.css'

import '../../fonts/blowbrush.ttf'

function BuzzUs() {

    return (
        <div id="maaaark" width="100%" style={{height: '100%'}}>
            <Card className="bg-dark text-white text-center" width="100%" style={{width:"100%"}}>
            <Card.Img src={Background} alt="Card image" width="10" style={{width:"100%"}}/>
            <Card.ImgOverlay style={{width:"100%"}}>
                <Card.Title><div className="title-text">Become a Buzzer</div></Card.Title>
                <Card.Text>
                <Container>
                    <Row style={{"padding-bottom": "15px"}}>
                        <Col xs={1} md={2}/>
                        <Col xs={10} md={8}>
                        <div className="text">
                            We are launching soon! Buzz us down here to get anexclusive invite to our first event of it's kind in the region! We cannot wait...
                        </div>
                        </Col>
                        <Col xs={1} md={2}/>
                    </Row>
                    <Row>
                        <Col/>
                        <Col>
                            <Form>
                                <Form.Group>
                                <Form.Control type="text" placeholder="First Name" />
                                </Form.Group>
                            </Form>
                        </Col>
                        <Col>
                            <Form>
                                <Form.Group>
                                <Form.Control type="text" placeholder="Last Name" />
                                </Form.Group>
                            </Form>
                        </Col>
                        <Col/>
                    </Row>
                    <Row>
                        <Col/>
                        <Col>
                            <Form>
                                <Form.Group>
                                <Form.Control type="email" placeholder="email"/>
                                </Form.Group>
                            </Form>
                        </Col>
                        <Col/>
                    </Row>
                    <Row>
                        <Col>
                            <a href="#" class="button">Hover me</a>
                            <p><button className="orange-button" type="submit">Buzz Us</button></p>
                        </Col>
                    </Row>
                </Container>
                
                

                
                
               
                
                </Card.Text>
            </Card.ImgOverlay>
            </Card>
        </div>
    )
}


export default BuzzUs;