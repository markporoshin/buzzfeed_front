import Background from '../../static/images/backgrounds/orange.jpg'
import {Card, Container, Col, Row} from 'react-bootstrap'

function OurStory() {

    return (
        <div id="maaaark" width="100%" style={{height: '100%'}}>
            <Card className="bg-dark text-white text-center" width="100%" style={{width:"100%"}}>
            <Card.Img src={Background} alt="Card image" width="10" style={{width:"100%"}}/>
            <Card.ImgOverlay style={{width:"100%"}}>
                <Card.Title><div className="title-text">Our Story</div></Card.Title>
                <Card.Text>
                <Container>
                    <Row>
                        <Col xs={1} md={2}/>
                        <Col xs={10} md={8}>
                        <div className="text">
                            We wanted to create an event that was a first in Dubai, where aside
                            from waking up and feeling physically good, people could also meet
                            other people, they could learn something new, they could walk away
                            each time with a story to share. And, with this story, other people
                            would feel tempted to attend our event, leaving with more stories
                            and suddenly this ripple effect becomes the buzz of the city, the ‘it’
                            place, the event to be at. Except, it does not involve any alcohol and
                            it happens only during mornings and we intentionally make it that
                            way.
                        </div>
                        </Col>
                        <Col xs={1} md={2}/>
                    </Row>
                </Container>
                </Card.Text>
            </Card.ImgOverlay>
            </Card>
        </div>
    )
}


export default OurStory;