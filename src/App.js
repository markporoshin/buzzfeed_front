import React, {useEffect, useState, useRef} from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import Header from './components/header/Header'
import Footer from './components/footer/Footer'
import Body from './components/body/Body'

import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  const [page, setPage] = useState("OurStory")
  const [wheel, setWheel] = useState(0)
  const menuCallback = (pageName) => {
    setPage(pageName)
  }

  const wheelHandler = (e) => {
      // console.log("wheel")
      // console.log("window.scrollY: " + window.scrollY)
      setWheel(window.scrollY)
      setPage("Undefined")
  }

  return (
    <Container fluid onWheel={(e)=>{wheelHandler(e)}}>
      <Row>
        <Col>
          <Header menuCallback={menuCallback} scroll={wheel}/>
        </Col>
      </Row>
      <Row style={{ "padding-left": 0, "padding-right": 0 }}>
        <Col style={{ "padding-left": 0, "padding-right": 0 }}>
          <Body page={page}/>
        </Col>
      </Row>
      <Row>
        <Col>
          <Footer/>
        </Col>
      </Row>
    </Container>
  );
}

export default App;
